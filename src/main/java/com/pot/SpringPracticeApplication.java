package com.pot;

import com.pot.model.Player;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class SpringPracticeApplication {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("file:src/main/java/com/pot/config.xml");

        Player player = (Player) context.getBean("player");
        System.out.println(player);

        SpringApplication.run(SpringPracticeApplication.class, args);
    }
}
