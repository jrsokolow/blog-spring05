package com.pot.model;

/**
 * Created by priv on 13.01.16.
 */
public class Player {

    private Console console;

    public Console getConsole() {
        return console;
    }

    public void setConsole(Console console) {
        this.console = console;
    }

    @Override
    public String toString() {
        return "This player plays on " + this.console.getName();
    }
}
